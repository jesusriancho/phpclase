<?php

// Crear un programa que nos cree 3 variables denominadas a,b,c. 
// Asignar 3 números aleatorios a las variables. 
// El programa nos debe indicar cuál de los 3 números es mayor

$a = mt_rand(1,10);
$b = mt_rand(1,10);
$c = mt_rand(1,10);
 
$mayor = max($a, $b, $c);
echo "El mayor de $a, $b y $c es $mayor";

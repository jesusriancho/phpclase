<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora de Nota Media</title>
    <style>
        table {
            border-collapse: collapse;
            width: 50%;
            margin: 20px;
        }
        table, th, td {
            border: 1px solid black;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
        th {
            background-color: #f2f2f2; /* Color de fondo para las celdas de encabezado */
        }
        .par{
            background-color: #f9f9f9; /* Color de fondo para filas pares */
        }
        .impar{
            background-color: #e6e6e6; /* Color de fondo para filas impares */
        }
    </style>
</head>
<body>

<?php
    // Definir las variables de las notas
    $nota1 = 7.5;
    $nota2 = 8.2;
    $nota3 = 6.8;
    $nota4 = 9.0;
    $nota5 = 7.3;

    // Calcular la nota media
    $notaMedia = ($nota1 + $nota2 + $nota3 + $nota4 + $nota5) / 5;
?>
<table><tr class="impar">
    <td><center> <h2>Notas del Alumno</h2> </center>
</tr>
    

    <table>
    <tr class="par">
            <th>Examen</th>
            <th>Nota</th>
        </tr>
        <tr class="impar">
            <td>Examen 1</td>
            <td><?= $nota1 ?></td>
        </tr>
        <tr class="par">
            <td>Examen 2</td>
            <td><?php echo $nota2; ?></td>
        </tr>
        <tr class="impar">
            <td>Examen 3</td>
            <td><?php echo $nota3; ?></td>
        </tr>
        <tr class="par">
            <td>Examen 4</td>
            <td><?php echo $nota4; ?></td>
        </tr>
        <tr class="impar">
            <td>Examen 5</td>
            <td><?php echo $nota5; ?></td>
        </tr>
        <tr class="par">
            <td><strong>Nota Media</strong></td>
            <td><strong><?php echo $notaMedia; ?></strong></td>
        </tr>
    </table>

</body>
</html>
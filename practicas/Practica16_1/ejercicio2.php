<?php

use clases\ejercicio2\Producto;
use clases\ejercicio2\Usuario;

spl_autoload_register(function ($clase) {
    require  $clase . ".php";
});


$usuario1 = new Usuario();
$usuario1->setIdUsuario(1);
$usuario1->setNombre("Pepe");
$usuario1->setEmail("pepe@pe.com");
$usuario1->setEdad(20);
$usuario1->setActivo(false);

echo $usuario1->mostrarInformacion();
$usuario1->activarUsuario();
$usuario1->cambiarEdad(25);

echo "<br>";
echo "<br>";
echo "CAMBIO DE EDAD Y ACTIVACIÓN";
echo "<br>";
echo "<br>";
echo $usuario1->mostrarInformacion();

echo "<br>";
echo "<br>";

$producto1 = new Producto();
$producto1->setIdProducto(1);
$producto1->setNombre("Cacao");
$producto1->setPrecio(2.5);
$producto1->setStock(10);
$producto1->setDescripcion("Onzas de cacao");

echo $producto1->mostrarDetalles();
echo "<br>";
echo "<br>";
echo "CAMBIO DE STOCK Y DESCUENTO DEL PRECIO";
echo "<br>";
echo "<br>";
$producto1->actualizarStock(5);

echo $producto1->mostrarDetalles();
echo $producto1->calcularPrecioDescuento(15);

<?php

namespace clases\ejercicio1;

class Persona
{

    public string $nombre;
    public int $edad;

    public function __construct($nombre, $edad)
    {
        $this->nombre = $nombre;
        $this->edad = $edad;
        
    }

  public function mostrarInformacion(): string{
    return "Nombre: " . $this->nombre . "  Edad:  ". $this->edad;
  }  
}
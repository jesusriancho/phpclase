<?php

use clases\ejercicio1\Persona;

spl_autoload_register(function ($clase) {
    require  $clase . ".php";
});

$persona1=new Persona("Pepe",41);

echo $persona1->mostrarInformacion();
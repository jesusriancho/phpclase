<?php

$a = 190; // para este valor debe imprimir "enorme"
// $a = 8;  // para este valor debe imprimir "poco"
// $a = 15; // para este valor debe imprimir "algo";
// $a = 25; // para este valor debe imprimir "medio"
// $a = 35; // para este valor debe imprimir "mucho"        
$b = ["poco", "algo", "medio", "mucho", "enorme"];

if ($a < 10) {
    echo $b[0];
} elseif ($a < 20) {
    echo $b[1];
} elseif ($a < 30) {
    echo $b[2];
} elseif ($a < 40) {
    echo $b[3];
} else {
    echo $b[4];
}

<?php

$a=10;
$b=3;

// el siguiente bloque pinta "punto 1" porque es par
if(($a%2)==0){
    echo "punto 1";
}else{
    echo "punto 2";
}

// el siguiente bloque pinta "punto 3"
echo "punto 3";

// el siguiente bloque pinta "punto 2" porque es impar
if(($b%2)==0){
    echo "punto 1";
}else{
    echo "punto 2";
}

// el siguiente bloque pinta "punto 3"
echo "punto 3";
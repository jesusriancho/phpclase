<?php

$letras = [
    "T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E", "T"
];

$dni = 72050163;
$letra = "M";

if ($dni < 0 || $dni > 99999999) {
    echo "El numero proporcionado no es válido";
} else {
    echo $letras[$dni % 23];
}
echo "<br>";

if ($letra != $letras[$dni % 23]) {
    echo "La letra introducida no es correcta";
} else {
    echo "El número y la letra son correctos";
}
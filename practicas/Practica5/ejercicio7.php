<?php

$dia = 6;

// utilizando if-else
echo "IF ";
if ($dia == 1) {
    echo "Lunes";
} elseif ($dia == 2) {
    echo "Martes";
} elseif ($dia == 3) {
    echo "Miercoles";
} elseif ($dia == 4) {
    echo "Jueves";
} elseif ($dia == 5) {
    echo "Viernes";
} elseif ($dia == 6) {
    echo "Sabado";
} elseif ($dia == 7) {
    echo "Domingo";
} else {
    echo "Error";
}
echo "<br>";

// utilizando switch 
echo "Switch ";
switch ($dia) {
    case 1:
        echo "Lunes";
        break;
    case 2:
        echo "Martes";
        break;
    case 3:
        echo "Miercoles";
        break;
    case 4:
        echo "Jueves";
        break;
    case 5:
        echo "Viernes";
        break;
    case 6:
        echo "Sabado";
        break;
    case 7:
        echo "Domingo";
        break;
    default:
        echo "Error";
        break;
}
echo "<br>";

// utilizando un array (pongo el domingo delante para que sea la posición 0)
echo "Array ";
$dias = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];

if (array_key_exists($dia, $dias)) {
    echo $dias[$dia];
} else {
    echo "Error";
}


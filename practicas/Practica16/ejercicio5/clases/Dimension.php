<?php

namespace clases;

class Dimension
{

    private float $alto;
    private float $ancho;
    private float $profundidad;

    public function __construct()
    {
        $this->alto = 0;
        $this->ancho = 0;
        $this->profundidad = 0;
    }

    /**
     * Get the value of alto
     */
    public function getAlto()
    {
        return $this->alto;
    }

    /**
     * Set the value of alto
     *
     * @return  self
     */
    public function setAlto($alto)
    {
        $this->alto = $alto;

        return $this;
    }

    /**
     * Get the value of ancho
     */
    public function getAncho()
    {
        return $this->ancho;
    }

    /**
     * Set the value of ancho
     *
     * @return  self
     */
    public function setAncho($ancho)
    {
        $this->ancho = $ancho;

        return $this;
    }

    /**
     * Get the value of profundidad
     */
    public function getProfundidad()
    {
        return $this->profundidad;
    }

    /**
     * Set the value of profundidad
     *
     * @return  self
     */
    public function setProfundidad($profundidad)
    {
        $this->profundidad = $profundidad;

        return $this;
    }

    public function getVolume(): float
    {
        return  $this->alto * $this->ancho * $this->profundidad;
    }

    public function __toString()
    {
        $salida = "Alto: " . $this->alto . ", Ancho: " . $this->ancho . ", Profundidad: " . $this->profundidad . ", Volumen: " . $this->getVolume();
        return $salida;
    }
}

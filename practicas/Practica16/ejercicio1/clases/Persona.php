<?php

namespace clases;

class Persona
{
    public string $nombre;
    public string $apellidos;
    public string $numeroDocumentoIdentidad;
    public int $anoNacimiento;

    public function __construct($nombre = "", $apellidos = "", $numeroDocumentoIdentidad = "", $anoNacimiento = 0)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->numeroDocumentoIdentidad = $numeroDocumentoIdentidad;
        $this->anoNacimiento = $anoNacimiento;
    }

    // metodo imprimir
    public function imprimir()
    {
        echo "Nombre = {$this->nombre} <br> Apellidos = {$this->apellidos} <br> Número de documento de identidad = {$this->numeroDocumentoIdentidad} <br> Año de nacimiento = {$this->anoNacimiento}";
    }
}

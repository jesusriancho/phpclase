<?php
use clases\Superheroe;

spl_autoload_register(function ($clase) {
    require  $clase . ".php";
});


$heroe1= new Superheroe("Batman");
$heroe1->setDescripcion("El mejor superheroe de todos los tiempos");
$heroe1->setCapa(true);


// cuando imprimo el objeto entonces
// llama al metodo mágico __toString()
// si el metodo toString no existe, produce una excepcion
echo $heroe1;

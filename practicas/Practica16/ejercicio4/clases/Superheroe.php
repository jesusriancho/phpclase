<?php

namespace clases;

class Superheroe
{
    private string $nombre;
    private string $descripcion;
    private bool $capa;

    public function __construct($nombre){
        $this->nombre = $nombre;
        $this->descripcion = "";
        $this->capa = false;
    }


    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */ 
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get the value of capa
     */ 
    public function getCapa()
    {
        return $this->capa;
    }

    /**
     * Set the value of capa
     *
     * @return  self
     */ 
    public function setCapa($capa)
    {
        $this->capa = $capa;

        return $this;
    }

    public function __toString():string
    {
        $salida="<h2>Datos</h2>";
        $salida.="<p>Nombre: ".$this->nombre."</p>";
        $salida.="<p>Descripción: ".$this->descripcion."</p>";
        $salida.="<p>Capa: ".($this->capa?"Si":"No")."</p>";
        return $salida; 
    }
}
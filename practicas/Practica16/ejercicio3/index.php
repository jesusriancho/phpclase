<?php

use clases\Planeta;

spl_autoload_register(function ($clase) {
    require  $clase . ".php";
});

$planeta1 = new Planeta("Venus", 0, 4.869 * 10 ** 24, 9.28 * 10 ** 11, 12103, 261 * 10 ** 6, "ENANO", true);

$planeta2 = new Planeta("Neptuno", 14, 1.024 * 10 ** 26, 6.254 * 10 ** 13, 49572, 4300 * 10 ** 6, "GAS", false);



echo $planeta1->imprimir();
echo $planeta1->calcularDensidad();
echo "<br>";
echo $planeta1->exterior();
echo "<br>";
echo "<br>";
echo $planeta2->imprimir();
echo $planeta2->calcularDensidad();
echo "<br>";
echo $planeta2->exterior();

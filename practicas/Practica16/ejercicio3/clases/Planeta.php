<?php

namespace clases;

class Planeta
{
    public ?string  $nombre;
    public int $cantidadSatelites;
    public float $masa;
    public float $kmCubicos;
    public int $kilometros;
    public int $distanciaSol;
    public string $tipoPlaneta;
    public bool $observable;

    public function __construct($nombre = null, $cantidadSatelites = 0, $masa = 0, $kmCubicos = 0, $kilometros = 0, $distanciaSol = 0, $tipoPlaneta = "", $observable = false)
    {
        $this->nombre = $nombre;
        $this->cantidadSatelites = $cantidadSatelites;
        $this->masa = $masa;
        $this->kmCubicos = $kmCubicos;
        $this->kilometros = $kilometros;
        $this->distanciaSol = $distanciaSol;
        $this->tipoPlaneta = $tipoPlaneta;
        $this->observable = $observable;
    }

    public function imprimir()
    {
        echo "Nombre = {$this->nombre} <br> Cantidad de satelites = {$this->cantidadSatelites}  <br> Masa = {$this->masa} <br> Kilo Cubicos = {$this->kmCubicos} <br> Distancia al Sol = {$this->kilometros} <br> Tipo de Planeta = {$this->tipoPlaneta} <br> Observable = {$this->getObservable()} <br>";
    }

    public function calcularDensidad()
    {

        echo "Densidad = " . $densidad = $this->masa / $this->kmCubicos;
    }

    public function exterior()
    {
        if ($this->distanciaSol > 3.4 * 149597870) {
            echo "El planeta es exterior";
        } else {
            echo "El planeta no es exterior";
        }
    }

    // añadimos un getter para $observable porque lo hace mal sino
    /**
     * Get the value of observable
     */
    public function getObservable(): string
    {
        $salida = $this->observable ? "Si" : "No";
        return $salida;
    }

   
}

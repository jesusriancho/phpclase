<?php

namespace clases;

use IntlChar;

class Persona
{
    public string $nombre;
    public string $apellidos;
    public string $numeroDocumentoIdentidad;
    public int $anoNacimiento;
    public string $pais;
    public string $sexo;

    public function __construct($nombre = "", $apellidos = "", $numeroDocumentoIdentidad = "", $anoNacimiento = 0, $pais = "", $sexo = "")
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->numeroDocumentoIdentidad = $numeroDocumentoIdentidad;
        $this->anoNacimiento = $anoNacimiento;
        $this->pais = $pais;
        $this->sexo = $sexo;
    }

    // metodo imprimir
    public function imprimir()
    {
        echo "Nombre = {$this->nombre} <br> Apellidos = {$this->apellidos} <br> Número de documento de identidad = {$this->numeroDocumentoIdentidad} <br> Año de nacimiento = {$this->anoNacimiento} <br> Pais = {$this->pais} <br> Sexo = {$this->sexo}";
    }
}

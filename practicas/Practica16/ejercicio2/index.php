<?php

use clases\Persona;

spl_autoload_register(function ($clase) {
    require  $clase . ".php";
});


$persona1 = new Persona("Pedro", "Perez", "1053121010", 1998, "España", "H");
$persona2 = new Persona("Juana", "Leon", "1053223344", 2001, "Suiza", "M");



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div>
        <div>p1=Persona</div>
        <div><?= $persona1->imprimir() ?></div>
    </div>
    <div>
        <div>p2=Persona</div>
        <div><?= $persona2->imprimir() ?></div>
    </div>
</body>
</html>
<?php
// el post está vacio 
// var_dump($_POST);

// el files contiene los metadatos de los ficheros
// var_dump($_FILES);

// controlar si he pulsado el boton
$mensaje = "";

if ($_FILES) {

    // creo un array con todos los metadatos de los 2 archivos
    $ficheroImg = $_FILES['fichero1'];
    $ficheroPdf = $_FILES['fichero2'];

    // ruta donde guardo los archivos subidos dentro de mi servidor web
    $rutaDestino1 = "./imgs/" . $ficheroImg['name'];
    $rutaDestino2 = "./pdf/" . $ficheroPdf['name'];

    // para mover el archivo de la carpeta temporal
    // donde lo coloca el servidor a la ruta destino
    $ficheroSubido1 = move_uploaded_file($ficheroImg['tmp_name'], $rutaDestino1);
    $ficheroSubido2 = move_uploaded_file($ficheroPdf['tmp_name'], $rutaDestino2);
    if ($ficheroSubido1 && $ficheroSubido2) {
        $mensaje = "ficheros subidos";
    } else {
        $mensaje = "ficheros no subidos";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post" enctype="multipart/form-data">
    <div>   
    <label for="text">Archivo 1:</label>
        <input type="file" name="fichero1" id="fichero1" multiple>
        </div> 
        <div>
        <label for="text">Archivo 2:</label>
        <input type="file" name="fichero2" id="fichero2" multiple>
        </div>
        <br>
        <button>Enviar</button>
    </form>
    <?= $mensaje ?>
</body>

</html>
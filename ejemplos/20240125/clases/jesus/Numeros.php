<?php

namespace clases\jesus;

class Numeros
{
    public array $numeros;
    public function __construct(array $numeros = [])
    {
        $this->numeros = $numeros;
    }

    // metodo para calcular la media de los numeros

    public function calcularMedia(): float
    {
        $media = 0;
        // utilizar la instruccion foreach
        foreach ($this->numeros as $numero) {
            $media += $numero;
        }
        // utilizar la funcion array_sum()
        $media = array_sum($this->numeros) / count($this->numeros);
        return $media;
    }

    public function calcularModa(): int
    {
        $frecuenciaMaxima = 0;
        $moda = 0;

        // opcion 1 
        // funcion calcularModa con array_count_values
        $frecuencia = array_count_values($this->numeros);
        foreach ($frecuencia as $indice => $frecuencia) {
            if ($frecuencia > $frecuenciaMaxima) {
                $frecuenciaMaxima = $frecuencia;
                $moda = $indice;
            }
        }

        // opcion 2
        // funcion calcularModa con array_count_values, max, array_search
        // $frecuencias = array_count_values($this->numeros);
        // $frecuenciaMaxima = max($frecuencias);
        // $moda = array_search($frecuenciaMaxima, $frecuencias);
        

        return $moda;
    }


    public function calcularMediana(): int
    {
        $mediana = 0;

        return $mediana;
    }
}

<?php

// importo la clase al espacio de nombres actual
use clases\jesus\Texto;
use clases\jesus\Numeros as ProfesorTexto;       


spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

// crear objeto de tipo texto

$objeto1 = new Texto();
$objeto2 = new  ProfesorTexto();

echo $objeto1->mostrar();



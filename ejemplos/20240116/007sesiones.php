<?php
// cambiar el nombre de la session
session_name('clase1');

// inicializamos la sesion
session_start();

$_SESSION['visitas'] = 1;
$_SESSION['nombre'] = 'ramon';
$_SESSION['apellido'] = 'sanchez';

// recuperar el nombre de la session
echo "nombre: " . session_name();
// recuperar el id de la sesion
echo "<br>id: " . session_id();

// recuperar el id de la session a traves de la cookie
var_dump($_COOKIE);

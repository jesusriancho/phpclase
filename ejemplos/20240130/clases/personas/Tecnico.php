<?php
// colocamos el espacio de nombres
namespace clases\personas;
// creamos la clase
class Tecnico
{
    // atributos de la clase
    // public / private / protected tipoDato identificador;
    public string $nombre;
    private string $iniciales;
    protected float $sueldo;
    public int $horas;

    // metodo constructor
    public function __construct(string $nombre) {
        $this->nombre = $nombre;
        $this->iniciales = "";
        $this->sueldo = 0;
        $this->horas = 0;
    }

    // metodos de la clase
    


    /**
     * devuelve las iniciales
     */ 
    public function getIniciales(): string
    {
        return $this->iniciales;
    }

    /**
     * asigna las iniciales
     *
     * @return  self
     */ 
    public function setIniciales($iniciales): self
    {
        $this->iniciales = $iniciales;

        return $this; // para trabajar en modo fluent
    }

    // metodo que calque el sueldo
    private function calculaSueldo():void {
        $this->sueldo = $this->horas * 10;
    }


    /**
     * Get the value of sueldo
     */ 
    public function getSueldo(): float
    {
        $this->calculaSueldo();
        return $this->sueldo;
    }
}

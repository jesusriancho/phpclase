<?php
namespace clases\personas;

// definicion de la clase

class Informatico extends Tecnico
{
  public string $aula;
  public array $ordenadores;

  // constructor

  public function __construct(){
    $this->aula = "";
    $this->ordenadores = [];
  }
}
<?php
// cargo la clase en el espacio de nombres actual
use clases\personas\Tecnico;

require_once "autoload.php";

$tecnico1 = new Tecnico("juan");
$tecnico1->horas = 100;

var_dump($tecnico1);
$tecnico1->setIniciales("jj"); // así asignas
echo $tecnico1->getIniciales(); // así accedes
echo "<br>";
// como está en fluent se pueden poner seguidas

echo $tecnico1->setIniciales("HH")
    ->getIniciales();

echo "<br>";

echo $tecnico1->getSueldo();

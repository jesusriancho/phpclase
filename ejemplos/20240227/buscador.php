<?php
// definiendo el autoload

use clases\Aplicacion;
use clases\Pagina;
use clases\Header;
use clases\Modelo;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$aplicacion = new Aplicacion();
$favoritos = new Modelo($aplicacion->db);
$favoritos->query("select * from favoritos where categorias='buscador'");

$aplicacion=new Aplicacion();
$cabecera=Header::ejecutar([
    "titulo" => "Pagina buscador",
    "subtitulo" => $aplicacion->configuraciones['autor'],
    "salida" => "Pagina buscador"
]);

Pagina::comenzar();
?>

<?= $favoritos->gridViewBotones(); ?>

<?php
Pagina::terminar(
    [
        "titulo" => "inicio",
        "cabecera" => $cabecera,
        "pie" => "Creado por: " . $aplicacion->configuraciones['autor']
    ]
);

<?php

namespace clases;


class Header extends Widget
{

   
    /**
     * Execute the PHP function with the given parameters and layout.
     *
     * @param array $parametros The parameters for the function
     * @param string $layout The layout for the function
     * @return string
     */
    public static function ejecutar(array $parametros = [], string $layout = 'componentes/jumbotron'): string
    {
        return parent::ejecutar($parametros, $layout);       
    }


    
    /**
     * terminar function
     *
     * @param array $parametros 
     * @param string $layout 
     * @throws 
     * @return void
     */
    public static function terminar(array $parametros = [], string $layout = 'componentes/jumbotron'): void
    {
        // termina el flujo de salida
        // para que no se muestre el HTML
        // en el navegador
        $salida = ob_get_clean();
        extract($parametros);
        require "layouts/" . $layout . ".php";
    }
}

<?php

namespace clases;

class Widget
{
    public static function comenzar()
    {
        // controla el flujo de salida
        // para que no se muestre el HTML
        // en el navegador
        ob_start();
    }

    /**
     * Executes the specified layout with the given parameters.
     *
     * @param array $parametros The array of parameters to be extracted and used in the layout.
     * @param string $layout The layout to be executed.
     * @throws \Exception If the specified layout file is not found.
     * @return void
     */
    public static function ejecutar(array $parametros = [], string $layout = 'layout1'): string
    {
        extract($parametros);
        ob_start();
        require "layouts/" . $layout . ".php";
        return ob_get_clean();
    }

    /**
     * metodo para mostrar la pagina con un determinado layout
     *
     * @param string $titulo el titulo a mostrar de la pagina
     * @param string $layout el nombre del layout a utilizar
     * @return string
     */
    public static function terminar(array $parametros = [], string $layout = 'layout1'): void
    {
        // termina el flujo de salida
        // para que no se muestre el HTML
        // en el navegador
        $contenido = ob_get_clean();
        extract($parametros);
        require "layouts/" . $layout . ".php";
    }
}

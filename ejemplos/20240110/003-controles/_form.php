<form method="POST">
    <div>
        <label for="numero1">Numero1</label>
        <input type="number" name="numero1" id="numero1" title="Primer numero" placeholder="Introduzca el primer numero" required>
    </div>
    <div>
        <label for="numero2">Numero2</label>
        <input type="number" name="numero2" id="numero2" title="Segundo numero" placeholder="Introduzca el segundo numero" required>
    </div>
    <div>
        <label for="numero3">Numero3</label>
        <input type="number" name="numero3" id="numero3" title="Tercer numero" placeholder="Introduzca el tercer numero" required>
    </div>
    <div>
        <button>Sumar</button>
    </div>
</form>
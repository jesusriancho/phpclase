<?php
// inicializar variables
$resultado="";
if ($_POST) {
    $resultado = $_POST['opciones'] ?? "";
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <select name="opciones" >
            <option value="car">Coche</option>
            <option value="bike" selected>bicicleta</option>
            <option value="motorbike">Motocicleta</option>
        </select>

        <button>Enviar</button>
    </form>
    <?= $resultado ?>
</body>
</html>
<?php

/**
 * Funcion que calcula una operacion
 * el primer numero en el tercer argumento y el segundo numero en el tercer argumento
 * y devolver las veces que se repiten y su suma
 * @param mixed $datos Array cuyo primner y segundo elemento son dos numeros y el tercer elemento un string en formato csv
 * @return array $resultado 
 * y en la segunda posición las veces que se repite el segundo numero
 * y en la tercera posición la suma de ambos    
 */
function operacion(...$datos) {
   
    $resultado = [0, 0 , 0];

    // buscamos el numero que esta en datos[0]
    // almacenamos el resultado en resultado[0]

    $resultado[0] = substr_count($datos[2], $datos[0]);
    
    // buscamos el numero que esta en datos[1]
    // almacenamos el resultado en resultado[1]

    $resultado[0] = substr_count($datos[2], $datos[1]);

    // sumar datos[0] y datos[1]
    // almacenar el resultado en resultado[2]

    $resultado[2] = $datos[0] + $datos[1];
   
    return $resultado;
    
}

if(isset($_POST['enviar'])) {
    $numero1 = $_POST['numero1'] ?: 0;
    $numero2 = $_POST['numero2'] ?: 0;
    $cadena = $_POST['cadena'] ?: "";

    // llamamos a la funcion
    $resultado = operacion($numero1, $numero2, $cadena);
}



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <div>
            <label for="numero1">Numero1</label>
            <input type="number" id="numero1" name="numero1" required>
        </div>
        <div>
            <label for="numero2">Numero2</label>
            <input type="number" id="numero2" name="numero2" required>
        </div> 
        <div>
            <label for="cadena">Operacion</label>
            <input type="text" id="cadena" name="cadena" required>
        </div> 
        <div>
            <button type="submit" name="enviar">Enviar</button>
            <button type="reset" name="limpiar">Limpiar</button>
        </div>  
    </form>
    <?php
    if(isset($_POST['enviar'])) {        
    ?>
    <h1>resultados</h1>              
    <p>El primer número <?= $numero1 ?> se repite: <?= $resultado[0] ?> veces</p>
    <p>El segundo número <?= $numero2 ?> se repite: <?= $resultado[1] ?> veces</p>
    <p>La suma de ambos es: <?= $resultado[2] ?></p>
    <?php
    }
    ?>
</body>
</html>
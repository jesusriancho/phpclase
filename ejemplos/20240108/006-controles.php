<?php

//inicializar variables
$nombre = "";
$apellidos = [];
$estado = "";
$aficiones = [];

// controlar que se hayan enviado los datos
if ($_POST) {
    // leyendo los datos
    $nombre = $_POST['nombre'] ?: "";
    $apellidos = implode(' ', $_POST['apellidos']); // separa el array en un string
    $estado = $_POST['estado'] ?: "";
    $aficiones = implode(',', $_POST['aficiones']);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    
    <form method="post">
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" id="nombre" name="nombre"></label>
        </div>
        <div>
            <label for="Apellido1">Apellido 1</label>
            <input type="text" id="apellidos" name="apellidos[]">
            <label for="Apellido1">Apellido 2</label>
            <input type="text" id="apellidos2" name="apellidos[]">
        </div>
        <div>
            <p>ESTADO CIVIL</p>
            <label for="casado">Casado/a</label>
            <input type="radio" id="casada" name="estado" value="casada">
            <label for="soltero">Soltero/a</label>
            <input type="radio" id="soltera" name="estado" value="soltera">
        </div>
        <div>
            <p>AFICIONES</p>
            <input type="checkbox" id="deportes" name="aficiones[]" value="deportes">
            <label for="deportes">Deporte</label>
            <input type="checkbox" id="leer" name="aficiones[]" value="leer">
            <label for="leer">Lectura</label>
            <input type="checkbox" id="dormir" name="aficiones[]" value="dormir">
            <label for="dormir">Dormir</label>
        </div>
        <br>
        <div>
            <button>Enviar</button>
            <button type="reset">Borrar</button>
        </div>
    </form>
    <?php
    if($_POST){
            ?>
            <div>
                <p>Nombre: <?php echo $nombre ?></p>
                <p>Apellidos: <?php echo $apellidos ?></p>
                <p>Estado civil: <?php echo $estado ?></p>
                <p>Aficiones: <?php echo $aficiones ?></p>
            </div>
    <?php
    }
    ?>
</body>

</html>
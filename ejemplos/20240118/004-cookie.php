<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">
        <div>
            <label for="number">Numero</label>
            <input type="number" name="numero" id="numero" title="introduce numero" placeholder="Introduce un numero"
                required>
        </div>
        <button>Enviar</button>
    </form>

    <br>

    <h1>DATOS INTRODUCIDOS</h1>
    <div class="etiqueta">
        <span class="etiqueta">Numero introducido actualmente</span> :
        <?= $_POST["numero"] ?? "" ?>
    </div>
    <div class="etiqueta">
        <span class="etiqueta">Numero introducido anteriormente</span> :
        <?= $_COOKIE["numeroC"] ?? "" ?>
    </div>

</body>

</html>
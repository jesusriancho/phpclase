<?php
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});


// instanciar un perro
$perro1 = new Perro();



// aprovecha el constructor y inicializo el nombre
$perro2 = new Perro("thor");

var_dump($perro1);
var_dump($perro2);

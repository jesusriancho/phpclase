<?php
class Persona
{
    // definir los miembros de mi clase

    // atributos de la clase
    public $nombre;
    public $apellidos;
    public $direccion;

    public $localidad;

    public $cp;

    // metodos de la clase

    public function hablar()
    {
        return "";
    }
    public function direccion()
    {
        // mediante $this podemos acceder a los miembros de la clase
        // dentro de la clase
        return "Direccion " . $this->direccion . "<br>Localidad " . $this->localidad . "<br>CP " . $this->cp;
    }

    // método que se ejecuta automaticamente cuando instancio la clase
   public function __construct()    {
       // inicializar los atributos de la clase
       $this->nombre = "";
       $this->apellidos = "";
       $this->direccion = "";
       $this->localidad = "";
       $this->cp = "";
   }     
}

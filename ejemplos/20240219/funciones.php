<?php

function controlErrores()
{
    // desactivar errores en pantalla
    // error_reporting(0);
    // convertir los errores de la libreria mysqli en warnings
    mysqli_report(MYSQLI_REPORT_OFF);
}


function gridViewBotones(mysqli_result $resultados)
{
    
    $registros = $resultados->fetch_all(MYSQLI_ASSOC);
    if (count($registros) > 0) {        
    
    // mostrar los registros
    $salida = "<table border='1'>";
    $salida .= "<thead><tr>";
    $campos = array_keys($registros[0]);
    foreach ($campos as $campo) {
        $salida .= "<td>$campo</td>";
    }
    // añado una columna para los botones
    $salida .= "<td>Acciones</td>";
    // cerramos la cabecera
    $salida .= "</tr></thead>";
    // muestro todos los registros
    foreach ($registros as $registro) {
        $salida .= "<tr>";
        // mostrando los campos
        foreach ($registro as $campo => $valor) {
            $salida .= "<td>" . $valor . "</td>";
        }
        // mostrando los botones
        $salida .= "<td>";

        // boton de editar
        $salida .= "<a href='actualizar.php?id=" . $registro['id'] . "' >Editar</a> | ";

        // boton de eliminar
        $salida .= "<a href='eliminar.php?id=" . $registro['id'] . "' >Eliminar</a>";

        // cerramos la celda
        $salida .= "</td>";

        $salida .= "</tr>";
    }
    $salida .= "</table>";
    $salida .= "<br>";
    // boton de insertar
    $salida .= "<a href='insertar.php'>Insertar nuevo registro</a>";
} else {
    $salida = "SIN REGISTROS";
    $salida .= "<br>";
    $salida .= "<br>";
    $salida .= "<br>";
    $salida .= "<a href='insertar.php'>Insertar nuevo registro</a>";
}
    return $salida;
}

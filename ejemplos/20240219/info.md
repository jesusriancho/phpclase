# Aplicacion para gestiona la tabla libros

Necesito desarrollar una aplicacion con php sin utilizar clases para realizar el CRUD de la tabla libros

## Caracteristicas de la aplicacion

- Menu en la parte superior de todas las paginas con lo siguiente
>Inicio: Nos muestra todos los registros

> Insertar

- Libreria con las funciones de
>control de errores

>gridviewBotones

## Tabla libros
Los campos de la tabla libros son
- id: entero, autoincremental
- titulo : texto de 200 caracteres
- paginas : entero
- fechaPublicacion: fecha

## Paginas del proyecto

Las paginas del proyecto seran:
- funciones.php
- _form : un solo formulario para actualizar e insertar
- index : donde sale el listado de todos los libros 
- insertar : para insertar un nuevo libro
- eliminar
- actualizar

### Pagina de Index

En la parte superior colocamos el menu y despues una tabla con todos los registros.

En cada registro un boton para editar y otro para eliminar.

Cuando pulso eliminar me elimina directamente el registro.

Cuando pulso actualizar me lleva al formulario para modificar el registro.

### Pagina de insertar

En la parte superior colocamos el menu.

Despues nos muestra el formuario para insertar un nuevo registro








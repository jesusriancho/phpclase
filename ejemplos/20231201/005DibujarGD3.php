<?php

header("Content-type: image/png");


// Dibujar un triángulo en la imagen
$x1 = mt_rand(10, 100); // Coordenada x del vértice superior
$y1 = mt_rand(10, 100); // Coordenada y del vértice superior
$x2 = mt_rand(10, 100); // Coordenada x del vértice inferior izquierdo
$y2 = mt_rand(10, 100); // Coordenada y del vértice inferior izquierdo
$x3 = mt_rand(10, 100); // Coordenada x del vértice inferior derecho
$y3 = mt_rand(10, 100); // Coordenada y del vértice inferior derecho

// dimensiones del lienzo
$width = 200;
$height = 200;
// creo lienzo dibujo
$imagen = imagecreatetruecolor($width, $height);
// coloco color fondo
$colorFondo = imagecolorallocatealpha($imagen, 177, 177, 177, 50);
// relleno el lienzo con el color de fondo
imagefill($imagen, 0, 0, $colorFondo);
// creo colores
$negro = imagecolorallocate($imagen, 0, 0, 0);
$rojo = imagecolorallocate($imagen, 255, 0, 0);
$verde = imagecolorallocate($imagen, 0, 255, 0);
$color1 = imagecolorallocatealpha($imagen, 0xC, 0xC, 0xC, 100);


//dibujo un triangulo como un poligono de 3 coordenadas
imagepolygon($imagen, [$x1, $y1, $x2, $y2, $x3, $y3], $rojo);

imagepng($imagen);


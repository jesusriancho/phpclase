<?php
// funcion que recibe como argumento el lado de un rectangulo y 
// devuelve el area de ese circulo
function areaRectangulo(int $lado): float
{
    return $lado**2;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Calcular el area de un circulo de area aleatoria</h1>
    <?php
    // genero un circulo de area aleatoria entre 10 y 150
    $lado = $_GET["lado"];

    // dibujo el circulo de ese radio
    // aprovecho el ejemplo anterior
    // mando el lado por get en la url

    echo "<img src='009DibujarGD.php?lado={$lado}'>";

    // calculo el area del circulo
    $area = areaRectangulo($lado);

    // mostrar el ladoy el area calculada
    ?>
    <ul>
        <li>Lado: <?= $lado?></li>
        <li>Area: <?= $lado?></li>
    </ul>


</body>

</html>
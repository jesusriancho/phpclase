<?php

$nota = 4;

// indicar si esta aprobado o suspenso
// si la nota es mayor o igual que 5, es aprobado en caso contraio suspenso

// if ($nota >= 5) {
//     echo "aprobado"; 
// } else {
//     echo "suspensos";
// }

// opcion 1

echo ($nota >= 5) ? "aprobado" : "suspensos";

// opcion 2

$resultado = ($nota >= 5) ? "aprobado" : "suspensos";
echo $resultado;


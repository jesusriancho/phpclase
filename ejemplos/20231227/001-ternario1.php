<?php

$dato = 10;
$valor = null;

?>

<h2> comprobar si existe y es distinta de null la variable</h2>

<?php

// La funcion isset comprueba si una variable existe y es distinta de null
// en caso de que no existe o que sea null devuelve falso
// si la variable vale false también devuelve false
// isset($dato); ==> true
// isset($dato1); ==> false
// isset($valor); ==> true
// isset(false); ==> false


// La funcioón empty comprueba si una variable existe y es distinta de null
// en caso de que no existe o que sea null devuelve true
// empty($dato); ==> false
// empty($dato1); ==> true
// empty($valor); ==> true
// empty(false); ==> true

// operador ternario recortado para comprobar si existe
echo $dato ?? 'no hay dato';
echo "br";
<?php
$a = [
    "nombre" => "ramon",
    "apellidos" => "abramo"
];

// devuelve la creaación del array como un string
$b = var_export($a, true);

// convierte un array a string con un caracter de separación
$c = implode(",", $a);

// crear variables con los indices del arraay asociativo
extract($a, EXTR_PREFIX_ALL, "Alpe");
// esto es equivalente
// $nombre=$a["nombre"];
// $apellidos=$a["apellidos"];

// esto realiza lo mismo que la funcion extract
// foreach ($a as $key => $value) {
//     $$key=$value;
// }

// mejor evitar usar la funcion extract

var_dump($b);
var_dump($c);
var_dump($Alpe_nombre);
var_dump($Alpe_apellidos);

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    $resultado = 0;
    $numero1 = 0;
    $numero2 = 0;
    // quiero que cuando pulse el botón de enviar,
    // me sume los 2 números y me coloque el resultado
    // Si no pulsa el botón de enviar, debe cargar el formulario
    // En caso de que los números estén vacíos, se supone 
    // que su valor es 0

    if (isset($_GET["enviar"])) {
        $numero1 = $_GET["numero1"] ?: 0;
        $numero2 = $_GET["numero2"] ?: 0;
        $resultado = $numero1 + $numero2;
        echo ($resultado);
    } else {
        require "_formulario2.php";
    }
    ?>
</body>
</html>
<?php
$datos = [
    "nombre" => "Pepe",
    "apellidos" => null,
    "edad" => null
];

// utilizando el operador if/else
// junto con la funcion isset
// realizar las siguiente asignaciones

// $nombre = "Pepe";
// $apellidos = "no conocidos";
// $edad = "no conocida";

if (isset($datos["nombre"]))

    $nombre = $datos["nombre"];
else {
    $nombre = "no conocido";
};
if (isset($datos["apellidos"]))
    $apellidos = $datos["apellidos"];
else {
    $apellidos = "no conocidos";
};
if (isset($datos["edad"]))
    $edad = $datos["edad"];
else {
    $edad = "no conocida";
};

var_dump($nombre);
var_dump($apellidos);
var_dump($edad);

// quiero realizar lo mismo pero utilizando ternario
$nombre= isset($datos['nombre']) ? $datos['nombre'] : 'no conocido';
   $apellidos= isset($datos['apellidos']) ? $datos['apellidos'] : 'no conocido';
   $edad= isset($datos['edad']) ? $datos['edad'] : 'no conocida';
   var_dump($nombre);
   var_dump($apellidos);
   var_dump($edad);

// quiero relizar lo mismo pero utilizando el operador ternario recortado

$nombre = $datos['nombre'] ?? 'no conocido';

var_dump($nombre);

$apellidos = $datos['apellidos'] ?? 'no conocido';

var_dump($apellidos);

$edad = $datos['edad'] ?? 'no conocida';

var_dump($edad);


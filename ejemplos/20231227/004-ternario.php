<?php

$datos = [
    "nombre" => "Pepe",
    "apellidos" => null,
    "edad" => null
];

// $nombre = "Pepe";
// $apellidos = "no conocidos";
// $edad = "no conocida";
// quiero realizar lo mismo con el ternario recortado

$nombre = $datos["nombre"] ?? "no conocido";
$apellidos = $datos["apellidos"] ?? "no conocido";
$edad = $datos["edad"] ?? "no conocida";

var_dump($nombre, $apellidos, $edad);

// quiero realizarlo con el operador ternario recortado elvis
// la diferencia es que si no existe, salta el warning

$nombre = $datos['nombre'] ?? 'no conocido';
$apellidos = $datos['apellidos'] ?? 'no conocido';
$edad = $datos['edad'] ?? 'no conocida';

// quiero realizarlo con el operador ternario recortado para null
<?php

namespace src;

class Empleado extends Persona
{
    private ?float $sueldo;

    // sobreescribir el metodo mostrar
    public function mostrar(): string
    {
        $salida = "<ul>";
        $salida .= "<li>Nombre:" . $this->getNombre() . "</li>";
        $salida .= "<li>Edad:" . $this->getEdad() . "</li>";
        $salida .= "<li>Sueldo:" . $this->sueldo . "</li>";
        $salida .= "</ul>";
        return $salida;
    }

    public function __construct()
    {
        parent::__construct();
        $this->sueldo = null;
        // en el constructor indico qué campos quiero 
        // que se asignen automáticamente desde un formulario
        $this->propiedadesAsignacionMasiva = ["nombre", "edad"];
    }
    public function calcularSueldo(int $horas): void
    {
        $this->sueldo = $horas * 10;
    }

    /**
     * Get the value of sueldo
     *
     * @return ?float
     */
    public function getSueldo(): ?float
    {
        return $this->sueldo;
    }

    // sobreescribir el metodo asignar
    public function asignar(array $datos): self
    {
        parent::asignar($datos);
        // asigna los campos que no estan en asignacion masiva
        $this->sueldo = $datos["horas"];
        return $this;
    }

}

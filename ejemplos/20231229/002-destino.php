<?php
// si no vienes del formulario, carga el formulario vacío
if (!$_POST) {
    header("Location: 002-formularioTresBotones.php");
} 


// $a = "ejemplo de clase";

//strlen($a); // suma los caracteres de la cadena

// si ponemos caracteres o tildes raras usamos esto
// mb_strlen($a);

//funcion que recibe un texto y un caracter
// y me tiene que indiciar el numero de veces que aparece el caracter
//en el texto

$palabra = $_POST["sumaLetras"] ?: "";
$cadena = $_POST["texto"] ?: "";
$resultado="";

// Ramon lo hace con una funcion

// function caracteres(string $texto, string $caracter): int
// {
//     $resultado = 0;

//     for ($i = 0; $i < strlen($texto); $i++) {
//         if ($texto[$i] == $caracter) {
//             $resultado++;
//         }
//     }
//     return $resultado;
// }



// switch ($valor) {
//     case "sum_a":
//         $resultado=caracteres($cadena, "a");
//         break;
//     case "sum_b":
//         $resultado=caracteres($cadena, "b");
//         break;
//     case "sum_c":
//         $resultado=caracteres($cadena, "c"); 
//         break;
//     default:
//         $resultado = "el caracter no es valido";
//         break;
// }


// yo lo hice con el case y el substr_count que es una funcion
// también se puede meter este switch en la función en lugar de la 
// que ha hecho Ramon

switch ($palabra) {
    case "a":
        $resultado=substr_count($cadena, "a");
        break;
    case "b":
        $resultado=substr_count($cadena, "b");
        break;
    case "c":
        $resultado=substr_count($cadena, "c"); 
        break;
    default:
        $resultado = 0;
        break;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?= "El numero de letras ". $palabra ." de "  . $cadena . "  es : " . $resultado ?>
</body>

</html>
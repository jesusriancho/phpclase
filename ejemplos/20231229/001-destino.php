<?php
// si no vienes del formulario, carga el formulario vacío
if (!$_POST) {
    header("Location: 001-formularioDosBotones.php");
} 


// no puedo recibir con _GET ya que en el formulario se pide el metodo POST
// var_dump($_GET);

// tendría que leerlo con post
// var_dump($_POST);

// puedo recibir siempre con request pero no es recomendable
// var_dump($_REQUEST); 

$operacion = $_POST["operacion"] ?: "";
$numero1 = $_POST["numero1"] ?: 0;
$numero2 = $_POST["numero2"] ?: 0;

switch ($operacion) {
    case "sumar":
        $resultado = $numero1 + $numero2;
        break;
    case "restar":
        $resultado = $numero1 - $numero2;
        break;
    default:
        $resultado = 0;
        break;
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?= "El resultado es: " . $resultado ?>
</body>

</html>
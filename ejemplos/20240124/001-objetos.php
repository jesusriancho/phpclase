<?php

spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . ".php";
});

// quiero introducir lo siguiente
//personas
// jorge, pedro y maria

$persona1 = new Humano("jorge","","28/03/1980");
$persona2 = new Humano("pedro");
$persona3 = new Humano("maria");

// oficios
// programador cobra 10€/hora y trabaja 50 horas
// ingeniero cobra 20€/hora y trabaja 50 horas

$oficio1 = new Oficio("programador",10,50);
$oficio2 = new Oficio("ingeniero",20,50);

// donde trabaja cada una
// jorge programador
// pedro ingeniero
// marta programador

$trabajan1 = new Trabajan($persona1, $oficio1);
$trabajan2 = new Trabajan($persona2, $oficio2);
$trabajan3 = new Trabajan($persona3, $oficio1);

echo $trabajan1->persona->presentarse();
echo "<br>Persona 1: ".$trabajan1->oficio->calcular();
﻿DROP DATABASE IF EXISTS personas;
-- Lenguaje de definición de datos

CREATE DATABASE personas;
USE personas;

CREATE TABLE persona(
  idPersona int AUTO_INCREMENT,
  nombre varchar(200),
  edad int,
  PRIMARY KEY (idPersona) 
);

-- Lenguaje de manipulación de datos

INSERT INTO persona
  (nombre, edad) VALUES
  ("persona1",18),
  ("persona2",45);

SELECT p.idPersona,p.nombre,p.edad FROM persona p;


CREATE TABLE cliente(
  idCliente int AUTO_INCREMENT,
  nombre varchar(200),
  edad int,
  nombreEmpresa varchar(200),
  telefono varchar(20),
  PRIMARY KEY (idCliente) 
);

-- Lenguaje de manipulación de datos

INSERT INTO cliente
  (nombre, edad, nombreEmpresa,telefono) VALUES
  ("cliente1",18, "Alpe", "942898989"),
  ("cliente2",33, "CIC","67890123");

SELECT c.idCliente, c.nombre, c.edad, c.nombreEmpresa, c.telefono FROM cliente c;


<?php

namespace clases;

class Persona
{
    // esta clase tiene que tener tantos atributos
    // como campos tenga la tabla
    public int $idPersona;
    public string $nombre;
    public int $edad;

    // el constructor si tu tienes esta clase
    // para fetch_object 
    // se llama despues de inicializar las propiedades
    public function __construct($datos = [])
    {
        if (count($datos) > 0) {
            foreach ($datos as $nombreCampo => $valorCampo) {
                if (property_exists($this, $nombreCampo)) {
                    $this->$nombreCampo = $valorCampo;
                }
            }
        }
    }

    // aqui pondria metodos que quiero que tenga esta clase
    public function __toString()
    {
        $salida = "<ul>";
        $salida .= "<li>Id Persona: " . $this->idPersona . "</li>";
        $salida .= "<li>Nombre: " . $this->nombre . "</li>";
        $salida .= "<li>Edad: " . $this->edad . "</li>";
        $salida .= "</ul>";
        return $salida;
    }
}

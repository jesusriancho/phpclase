<?php

// objetivo

// conectarme al servidor de base de datos mysql localhost
// seleccionar la base de datos personas
// realizar una consulta para mostrar la tabla Cliente
// mostrar los registros de la tabla cliente
// cerrar conexión

// crear una conexion con el servidor utilizando la 
// clase mysqli

$conexion = new mysqli(
    'localhost', // host al que quiero conectarme
    'root', // usuario de la base de datos
    '', // contraseña
    'personas' // base de datos por defecto, esto es opcional
);

// la variable conexion es un OBJETO de tipo mysqli


// realizar una consulta para mostrar la tabla persona  
$resultados = $conexion->query("SELECT c.idCliente, c.nombre, c.edad, c.nombreEmpresa, c.telefono FROM cliente c;");
// la variable resultado es un OBJETO de tipo mysqli_result


// mostrar los registros de la tabla persona

$registros = $resultados->fetch_all(MYSQLI_ASSOC);
// registros es una variable de tipo array BIDIMENSIONAL (enumerado en la primera dimension y asociativo en la segundo)

// muestro el array
// con un foreach
foreach ($registros as $indice => $registro) {
?>
    <h2>Registro <?= $indice ?> </h2>
    <ul>
        <li>idCliente: <?= $registro['idCliente'] ?></li>
        <li>nombre: <?= $registro['nombre'] ?></li>
        <li>edad: <?= $registro['edad'] ?></li>
        <li>nombreEmpresa: <?= $registro['nombreEmpresa'] ?></li>
        <li>telefono: <?= $registro['telefono'] ?></li>
    </ul>

<?php
}

$conexion->close();




<?php
// crear dos variables con numeros enteros
$num1 = 0;
$num2 = 0;
$salida = "";

// cargar las variables con los datos del formulario
$num1 = $_GET["numero1"];
$num2 = $_GET["numero2"];


// quiero una variable salida que me indique
// si num1 es mayor que num2
// si num2 es mayor que num1
// o si son iguales

if ($num1 > $num2) {
    $salida = "El numero1 {$num1} es mayor que el numero2 {$num2}";
} elseif ($num2 > $num1) {
    $salida = "El numero2 {$num2} es mayor que el numero1 {$num1}";
} else {
    $salida = "El numero1 {$num1} y el numero2 {$num2} son iguales";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div>
        <?= $salida ?>
    </div>
</body>

</html>
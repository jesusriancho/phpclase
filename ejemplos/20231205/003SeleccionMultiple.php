<?php
// crear dos variables con numeros enteros
$num1 = 20;
$num2 = 20;
$salida = "";

// quiero una variable salida que me indique
// si num1 es mayor que num2
// si num2 es mayor que num1
// o si son iguales

if ($num1 > $num2) {
    $salida = "El numero1 {$num1} es mayor que el numero2 {$num2}";
} elseif ($num2 > $num1) {
    $salida = "El numero2 {$num2} es mayor que el numero1 {$num1}";
} else {
    $salida = "El numero1 {$num1} y el numero2 {$num2} son iguales";
}

echo $salida;

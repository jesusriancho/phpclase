<?php
use clases\animales\Animal;
use clases\animales\Perro;
use clases\animales\Persona;
use clases\animales\Vaca;

include "autoload.php";

$animal1=new Animal("perro",25.4,"blanco");
$persona1=new Persona("Pedro","berta perogordo",942898989);
$perro1=new Perro("galleta",3.4,"negro",true,$persona1);
$vaca1=new Vaca("Tula",89,"pinta","Suances","Tio Tom");


var_dump($animal1);
var_dump($perro1);
var_dump($persona1);
var_dump($vaca1);
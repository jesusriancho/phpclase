<?php

include "autoload.php";

use clases\numero\Numeros;
use clases\texto\Cadenas;

$objeto1 = new Numeros(45);
$objeto2 = new Cadenas("Hola Clase");

var_dump($objeto1);
var_dump($objeto2);

<?php

// opcion 1
use clases\herencia\Animal;
use clases\herencia\Perro;
use clases\herencia\Persona;
use clases\herencia\Vaca;

// opcion 2
// use clases\herencia\{Perro, Vaca, Animal, Persona};

require_once "autoload.php";
$vaca1 = new Vaca("Ernesta");
$perro1 = new Perro();
$perro2= new Perro("Tito");
$animal1= new Animal();

$persona1 = new Persona();

var_dump($vaca1);
var_dump($perro1);
var_dump($perro2);
var_dump($animal1);
var_dump($persona1);

echo $perro2->descripcion(); // llama al metodo descripcion de la clase Perro, que hereda de Animal
echo "<br>";

// llama al metodo descripcion de la clase Vaca
// como la clase vaca tiene ese metodo lo utiliza
// la clase Animal tambien tiene ese metodo pero la clase vaca lo sobreescribe
echo $vaca1->descripcion();
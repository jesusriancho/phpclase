<?php

namespace clases\numero;

class Numeros
{
    // atributos
    public int $valor;

    public function __construct(int $valor = 0)
    {
        $this->valor = $valor;
    }
}

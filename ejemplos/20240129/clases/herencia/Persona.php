<?php

namespace clases\herencia;

class Persona
{
    // atributos
    public string $nombre;
    public string $direccion;
    public int $telefono;

    public function __construct(string $nombre = "", string $direccion = "", int $telefono = 0)    
    {
        $this->nombre = $nombre;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
    }

   
}
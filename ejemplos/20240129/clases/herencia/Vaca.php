<?php

namespace clases\herencia;

class Vaca extends Animal // vaca hereda las propiedades de animal
{
    // atributos

    public string $direccion;
    public string $granja;


    public function __construct(string $nombre = "", float $peso = 0, string $color = "", string $direccion = "", string $granja = "")
    {
        $this->nombre = $nombre;
        $this->peso = $peso;
        $this->color = $color;
        $this->direccion = $direccion;
        $this->granja = $granja;
    }

    public function descripcion(): string
    { // llamo al metodo descripcion de la clase animal
       return parent::descripcion(). " y vive en la granja {$this->granja}.";
        //return "La vaca llamada {$this->nombre} tiene {$this->peso} kilos y es de color {$this->color} y vive en {$this->direccion} de la granja {$this->granja}.";
    }
}

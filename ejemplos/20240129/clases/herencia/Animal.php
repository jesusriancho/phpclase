<?php

namespace clases\herencia;

class Animal // esta va a ser la clase padre (superclase)
{
    // atributos
    public string $nombre;
    public float $peso;
    public string $color;

    public function __construct(string $nombre = "animal", float $peso = 4.5, string $color = "blanco")
    {
        $this->nombre = strtoupper($nombre);// convierte a mayusculas
        $this->peso = $peso;
        $this->color = $color;
    }

    public function descripcion(): string{
        return "{$this->nombre} tiene {$this->peso} kilos y es de color {$this->color}";
    }
}

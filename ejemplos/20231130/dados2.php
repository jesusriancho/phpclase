<?php
$tiradas = []; //array vacío
// Simular una tirada
$tirada = mt_rand(1, 6); //Devuelve un número entero random entre 1 y 6
// Simular 3 tiradas de 2 dados con un array asociativo Haciendo un push
//$tiradas[0] ["dado1"] = mt_rand(1,6);
//$tiradas[0] ["dado2"]= mt_rand(1,6);
//$tiradas[1] ["dado1"] = mt_rand(1,6);
//$tiradas[1] ["dado2"]= mt_rand(1,6);
//$tiradas[2] ["dado1"] = mt_rand(1,6);
//$tiradas[2] ["dado2"]= mt_rand(1,6);

// Otra manera de hacerlo es poner un array asociativo dentro de otro
$tiradas[] = [
    "dado1" => mt_rand(1, 6),
    "dado2" => mt_rand(1, 6)
];

$tiradas[] = [
    "dado1" => mt_rand(1, 6),
    "dado2" => mt_rand(1, 6)
];

$tiradas[] = [
    "dado1" => mt_rand(1, 6),
    "dado2" => mt_rand(1, 6)
];

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <ul>
        <li><?= $tiradas[0]["dado1"] ?></li>
        <li><?= $tiradas[0]["dado2"] ?></li>
        <div><img src="dados/<?= $tiradas[0]["dado1"] ?>.svg">
        <img src="dados/<?= $tiradas[0]["dado2"] ?>.svg">
    </div>
    </ul>
    <ul>
        <li><?= $tiradas[1]["dado1"] ?></li>
        <li><?= $tiradas[1]["dado2"] ?></li>
        <div><img src="dados/<?= $tiradas[1]["dado1"] ?>.svg">
        <img src="dados/<?= $tiradas[1]["dado2"] ?>.svg">
    </div>
    </ul>
    <ul>
        <li><?= $tiradas[2]["dado1"] ?></li>
        <li><?= $tiradas[2]["dado2"] ?></li>
        <div><img src="dados/<?= $tiradas[2]["dado1"] ?>.svg">
        <img src="dados/<?= $tiradas[2]["dado2"] ?>.svg">
    </div>
    </ul>

    

    

    


    <?php
    // ahora se dibujan los dados con img según las tiradas. Para ello uso la impresion corta
    ?>

</body>

</html>
<?php
$tiradas = [];//array vacío
// Simular una tirada
$tirada=mt_rand(1,6); //Devuelve un número entero random entre 1 y 6
// Simular 3 tiradas de 1 dado con un array Haciendo un push
$tiradas[] = mt_rand(1,6);
$tiradas[] = mt_rand(1,6);
$tiradas[] = mt_rand(1,6);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <ul>
        <li><?= $tiradas[0] ?></li>
        <li><?= $tiradas[1] ?></li>
        <li><?= $tiradas[2] ?></li>
    </ul>

<?php
// ahora se dibujan los dados con img según las tiradas. Para ello uso la impresion corta
?>
<img src="dados/<?= $tiradas[0]?>.svg">
<img src="dados/<?= $tiradas[1]?>.svg">
<img src="dados/<?= $tiradas[2]?>.svg">
</body>
</html>
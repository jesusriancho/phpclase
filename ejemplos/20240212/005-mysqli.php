<?php
// -- UTILIZAMOS FETCH_ALL --

// mostrar todos los datos de la tabla personas
// de la base de datos personas1
// del servidor de base de datos que esta en localhost

// Quiero que utilicemos el metodo fetch_all
// utilizando un array asociativo para cada registro

// cargo la libreria con funciones para mostrar informacion
require_once "funcionesObjetos.php";
require_once "controlErrores.php";

// Definir la información de conexión a la base de datos
$host = "localhost";
$usuario = "root";
$contrasena = "";
$base_de_datos = "personas1";

// 1 paso 
// realizar la conexion
$conexion = @new mysqli($host, $usuario, $contrasena, $base_de_datos);

// 2 paso 
// comprobar que la conexion se ha realizado correctamente
if ($conexion->connect_error) {
    die("La conexion ha fallado: " . $conexion->connect_error);
}

// 3 paso
// realizar la consulta
$consulta = "select * from personas";
$resultados = $conexion->query($consulta);
// me retorna un objeto de tipo mysqli_result

// llamo a un gridviewtable y le paso la consulta
// sin ejecutar como mysqli_result
echo gridViewTable($resultados);

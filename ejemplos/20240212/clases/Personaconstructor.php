<?php

namespace Clases;

class Personaconstructor
{

    public int $id;
    public string $nombre;
    public string $apellidos;
    public int $edad;
    public string $fechaNacimiento;
    public string $poblacion;
    public string $codigoPostal;

    public function __toString()
    {
        
    }

    public function __construct(
        int $id,
        string $nombre,
        string $apellidos,
        int $edad,
        string $fechaNacimiento,
        string $poblacion,
        string $codigoPostal
    ) {

        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->edad = $edad;
        $this->fechaNacimiento = $fechaNacimiento;
        $this->poblacion = $poblacion;
        $this->codigoPostal = $codigoPostal;
    }
}

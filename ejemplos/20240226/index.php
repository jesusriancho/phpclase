<?php
// arranco sesiones
session_start();

// cargo las funciones
require_once "funciones.php";

// control de errores
controlErrores();

// inicializo las variables
$salida = "";
$parametros = require_once "parametros.php";
$menu = "";

// compruebo si he pulsado el boton de login
if ($_POST) {
    // creo una variable de sesion para comprobar que estoy logueado
    $_SESSION['nombre'] = $_POST['nombre'];
}

// compruebo si estoy logueado
if (isset($_SESSION['nombre'])) {
    $menu = menu([
        "Inicio" => "index.php",
        "Mensaje" => "mensaje.php",
        "Salir" => "salir.php",
    ]);

    $salida = "Bienvenido {$_SESSION['nombre']}";
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="css/home.css">

</head>

<body>

    <?= $menu ?>
    <div class="container">
        <div class="py-5 bg-dark text-white text-center">
            <i class="bi bi-person-circle" style="font-size: 4rem;"></i>
            <h1 class="display-4">Inicio</h1>
            <p class="lead">Aplicacion de ejemplo para mensajes con login</p>
        </div>
        <div class="p-5 bg-dark text-white">
            <div class="lead"><?= $salida ?></div>
        </div>
    </div>

    <?php
    if (!isset($_SESSION['nombre'])) {
        require "_login.php";
    }
    ?>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>
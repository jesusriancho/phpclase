<?php
//creo la variable y asigno un valor
$texto="ejemplo de clase";
$a=10;
$b=2.34;
$c= true;
$d= null;
$e= "";
$f ;//salida por pantalla
echo $texto;

// con el var_dump se hace un debugeo de la variable, no una impresion
var_dump("texto=",$texto);

// ahora depuramos todas las variables
var_dump($a,$b,$c,$d,$e,$f);
<?php
// cargar la libreria 001-arrays.php

require_once('001-arrays.php');

// cargar los datos que estan en datos.php
require_once('datos.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Formas de recorrer el array</h1>
    <h2>FOR</h2>
    <?= recorrer($ciudades) ?>
    <?= recorrer($edades) ?>
    <h2>FOR CON ARRAY_VALUES</h2>
    <?= recorrerV1($ciudades) ?>
    <?= recorrerV1($edades) ?>
    <?= recorrerV1($valores) ?>
    <h2>FOREACH</h2>
    <?= recorrer2($ciudades) ?>
    <?= recorrer2($edades) ?>
    <?= recorrer2($valores) ?>
    <h2>DO WHILE</h2>
    <?= recorrer3($ciudades) ?>
    <?= recorrer3($edades) ?>
    <?= recorrer3($valores) ?>
    <h2>ARRAY_WALK</h2>
    <?= recorrer4($ciudades) ?>
    <?= recorrer4($edades) ?>
    <?= recorrer4($valores) ?>
    <?= recorrer4($colores) ?>
    <h2>ARRAY_MAP</h2>
    <?= recorrer5($ciudades) ?>
    <?= recorrer5($edades) ?>
    <?= recorrer5($valores) ?>
    <?= recorrer5($colores) ?>
</body>

</html>
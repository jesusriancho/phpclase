<?php

$alumnos = [
    [
        "id" => 1,
        "nombre" => "Pepe",
        "apellidos" => "Perez",
        "edad" => 20
    ],
    [
        "id" => 2,
        "nombre" => "Juan",
        "apellidos" => "Lopez",
        "edad" => 21
    ]
];

$menu =
    [
        "Home" => "www.alpeformacion.es",
        "Donde estamos" => "pepito.php",
        "Quienes somos" => "#",
        "mas informacion" =>
        [
            "Contacto" => "contacto.php",
            "Alumnos" => "alumnos.php",
        ]
    ];


//  <ul>
//     <li><a href="www.alpeformacion.es">Home</a>"</li>
//     <li><a href="pepito.php">Donde estamos</a>"</li>
//     <li><a href="#">Quienes somos</a>"</li>
//     <li>Mas informacion
//         <ul>
//             <li><a href="contacto.php">Contacto</a>"</li>
//             <li><a href="alumnos.php">Alumnos</a>"</li>
//         </ul>
//     </li>
// </ul> 
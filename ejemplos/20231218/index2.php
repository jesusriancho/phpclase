<?php
// cargar la libreria 002-arrays.php

require_once('002-arrays.php');

// cargar los datos que estan en datos.php
require_once('datos.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
<h1>Formas de mostraArray el array</h1>
    <h2>FOR</h2>
    <?= mostraArray($ciudades) ?>
    <?= mostraArray($edades) ?>
    <h2>FOR CON ARRAY_VALUES</h2>
    <?= mostraArrayV1($ciudades) ?>
    <?= mostraArrayV1($edades) ?>
    <?= mostraArrayV1($valores) ?>
    <h2>FOREACH</h2>
    <?= mostraArrayV2($ciudades) ?>
    <?= mostraArrayv2($edades) ?>
    <?= mostraArrayV2($valores) ?>
    <h2>ARRAY_WALK</h2>
    <?= mostraArrayV3($ciudades) ?>
    <?= mostraArrayV3($edades) ?>
    <?= mostraArrayV3($valores) ?>
    <?= mostraArrayV3($colores) ?>
    <h2>ARRAY_MAP</h2>
    <?= mostraArrayV4($ciudades) ?>
    <?= mostraArrayV4($edades) ?>
    <?= mostraArrayV4($valores) ?>
    <?= mostraArrayV4($colores) ?>
</body>
</html>
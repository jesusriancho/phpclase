<?php

// crear una funcion llamda recorrer
// que reciba como primer parametro un array
// deber devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar for
// observaciones
// solo debe funcionar con arrays una sola dimension
// solo son arrays enumerados secuenciales


function recorrer(array $datos): string
{
    $lista = "<ul>";
    for ($i = 0; $i < count($datos); $i++) {
        $lista .= "<li>{$datos[$i]}</li>";
    }
    $lista .= "</ul>";
    return $lista;
}


// crear una funcion llamda recorrerV1
// que reciba como primer parametro un array
// deber devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar for
// observaciones
// solo debe funcionar con arrays una sola dimension
// solo son arrays enumerados tanto secuenciales como no secuenciales
// pista: array_values

function recorrerV1(array $datos): string
{
    $datos = array_values($datos);
    $lista = "<ul>";
    for ($i = 0; $i < count($datos); $i++) {
        $lista .= "<li>{$datos[$i]}</li>";
    }

    $lista .= "</ul>";

    return $lista;
}


// crear una funcion llamda recorrer2
// que reciba como primer parametro un array
// deber devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar foreach
// observaciones
// solo debe funcionar con arrays una sola dimension
// solo son arrays asociativos y enumerados

function recorrer2(array $datos): string
{
    $lista = "<ul>";
    foreach ($datos as $valor) {
        $lista .= "<li>{$valor}</li>";
    }
    $lista .= "</ul>";
    return $lista;
}

// crear una funcion llamda recorrer3
// que reciba como primer parametro un array
// deber devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar while
// observaciones
// solo debe funcionar con arrays una sola dimension
// solo son arrays enumerados y asociativos

function recorrer3(array $datos): string
{
    // creamos un array enumerado secuencial
    $datos = array_values($datos);
    $lista = "<ul>";
    $i = 0;
    while ($i < count($datos)) {
        $lista .= "<li>{$datos[$i]}</li>";
        $i++;
    }
    $lista .= "</ul>";
    return $lista;
}


// crear una funcion llamda recorrer4
// que reciba como primer parametro un array
// deber devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar array_walk

function recorrer4(array $datos): string
{
    // creamos un array enumerado secuencial
    
    $lista = "<ul>";
        array_walk($datos, function($valor, $indice) use (&$lista) {
            $lista .= "<li>{$valor}</li>";
        });
    $lista .= "</ul>";
    return $lista;
}

// crear una funcion llamda recorrer4V1
// que reciba como primer parametro un array
// deber devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar array_map 
// con array_walk añadir a cada elemento del array una etiqueta <li>valor</li>
// para unir el array nuevo utilizar implode



// crear una funcion llamda recorrer5
// que reciba como primer parametro un array
// deber devolver una lista de html con todos los elementos del array
// para recorrer el array utilizar array_map 
// con array_map añadir a cada elemento del array una etiqueta <li>valor</li>
// para unir el array nuevo utilizar implode

function recorrer5(array $datos): string
{
    // creamos un array enumerado secuencial
    $lista = "<ul>";
    $datos1=array_map(function($valor) {
        return "<li>{$valor}</li>";
    }, $datos);
    $lista .= implode("",$datos1);
    $lista .= "</ul>";
    return $lista;
}


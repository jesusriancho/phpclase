<?php
$a = 10;
$b = 3;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // Quiero que coloquemos los resultados de las siguientes operaciones
    // a+b
    // a-b
    // a*b
    // a resto b
    // a elevado a b
    // a dividido entre b
    // a es igual a b
    // a menor igual que b

    // colocamos una tabla en donde la primera coolumna es el nombre 
    // de la operacion
    // en la segunda columna el resultado

    // procesamiento
    $suma = $a+$b;
    $resta = $a-$b;
    $producto = $a*$b;
    $cociente = $a/$b;
    $potencia = pow($a,$b);
    if ($a == $b) {
        $iguales = "Son iguales";
    } else {
        $iguales = "Son diferentes";
    };
    if ($a >= $b) {
        $menor = "Son iguales";
    } else {
        $menor = "Son diferentes";
    };
    ?>
    <table border="1">
        <tr>
            <td>Sumar</td>
            <td><?php echo $suma ?></td>
        </tr>
        <tr>
            <td>Restar</td>
            <td><?php echo $resta ?></td>
        </tr>
        <tr>
            <td>Multiplicacion</td>
            <td><?php echo $producto ?></td>
        </tr>
        <tr>
            <td>Potencia</td>
            <td><?php echo $potencia ?></td>
        </tr>
        <tr>
            <td>Cociente</td>
            <td><?php echo $cociente ?></td>
        </tr>
        <tr>
            <td>A igual a B</td>
            <td><?= $iguales
                
                ?>
            </td>
        </tr>
        <tr>
            <td>A es menor que B</td>
            <td><?= $menor
                ?></td>
        </tr>

    </table>
    </tr>
</body>

</html>
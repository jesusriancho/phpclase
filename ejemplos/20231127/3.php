<?php
$a = 10;
$b = 3;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // Quiero que coloquemos los resultados de las siguientes operaciones
    // a+b
    // a-b
    // a*b
    // a resto b
    // a elevado a b
    // a dividido entre b
    // a es igual a b
    // a menor igual que b

    // colocamos una tabla en donde la primera coolumna es el nombre 
    // de la operacion
    // en la segunda columna el resultado

    //

    ?>
    <table border="1">
        <tr>
            <td>Sumar</td>
            <td><?= $a + $b ?></td>
        </tr>
        <tr>
            <td>Restar</td>
            <td><?= $a - $b ?></td>
        </tr>
        <tr>
            <td>Multiplicacion</td>
            <td><?= $a * $b ?></td>
        </tr>
        <tr>
            <td>Potencia</td>
            <td><?= pow($a, $b) ?></td>
        </tr>
        <tr>
            <td>Cociente</td>
            <td><?= $a / $b ?></td>
        </tr>
        <tr>
            <td>A igual a B</td>
            <td><?php
                if ($a == $b) {
                    echo "Son iguales";
                } else {
                    echo "Son diferentes";
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>A es menor que B</td>
            <td><?php
                if ($a <= $b) {
                    echo "a es menor que b";
                } else {
                    echo "a es mayor que b";
                }
                ?></td>
        </tr>

    </table>
    </tr>
</body>

</html>
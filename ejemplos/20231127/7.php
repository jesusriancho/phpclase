<?php
$a = 10;
$b = 3;
$salida=[]; //inicializar siempre el array por si hay que añadir elementos al array

// Quiero que coloquemos los resultados de las siguientes operaciones
    // a+b
    // a-b
    // a*b
    // a resto b
    // a elevado a b
    // a dividido entre b
    // a es igual a b
    // a menor igual que b

    

$salida = [
    "suma" => ($a + $b),
    "resta" => ($a - $b),
    "multiplicacion" => ($a * $b),
    "division" => ($a / $b),
    "potencia" => (pow($a,$b)),
  
];

if ($a == $b) {
    $salida["igual"] = "Son iguales";
    } else {
    $salida["igual"] = "Son diferentes";
    };
if ($a <= $b) {
    $salida["menor"] = "Son iguales";
} else {
    $salida["menor"] = "Son diferentes";
};
?>

<table border="1">
    <tr>
        <td>Operacion</td>
        <td>Resultado</td>
    </tr>
    <tr>
        <td>suma</td>
        <td><?= $salida["suma"] ?></td>
    </tr>
    <tr>
        <td>resta</td>
        <td><?= $salida["resta"] ?></td>
    </tr>
    <tr>
        <td>multiplicacion</td>
        <td><?= $salida["multiplicacion"] ?></td>
    </tr>
    <tr>
        <td>division</td>
        <td><?= $salida["division"] ?></td>
    </tr>
    <tr>
        <td>potencia</td>
        <td><?= $salida["potencia"] ?></td>
    </tr>
    <tr>
        <td>igual</td>
        <td><?= $salida["igual"] ?></td>
    </tr>
    <tr>
        <td>menor</td>
        <td><?= $salida["menor"] ?></td>
    </tr>
</table>
<?php
// se supone que abro una base de datos quiero los datos del alumno con la nota más alta

// hacemos una array asociativo

$datos=[
    "id"=>12,
    "nombre" => "Eva",
    "apellidos" => "Gomez Palomo",
    "poblacion" => "Laredo"
];

?>

<table border="1">
    <tr>
        <td>Campo</td>
        <td>Valor</td>
    </tr>
    <tr>
        <td>id</td>
        <td><?=$datos["id"]?></td>
    </tr>
    <tr>
        <td>nombre</td>
        <td><?=$datos["nombre"]?></td>
    </tr>
    <tr>
        <td>apellidos</td>
        <td><?=$datos["apellidos"]?></td>
    </tr>
    <tr>
        <td>poblacion</td>
        <td><?=$datos["poblacion"]?></td>
    </tr>
</table>
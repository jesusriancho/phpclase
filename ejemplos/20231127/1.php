<?php
$a=1;
$b= 2.3;

//crear variable de tipo string

$c= "ejemplo";
$d= "1";

//crear variable booleana

$e= true;
$f= false;

//crear variable array

$g= [1,2,"hola"];
$h= array(23,3);

//crear variable tipo null

$i=null;

// para comprobar valor y tipo 
// usamos var_dump

var_dump($a, $d, $e, $h);

// para comprobar el tipo, se usa gettype

echo gettype($d);

// para comprobar el valor, usamos echo o la impresión corta

//echo "<div"


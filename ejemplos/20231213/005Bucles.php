<?php

// creo una variable llamada numero y le coloco un numero entero
// esta variable debe tener un entero entre 1 y 20
$numero = 18;

// imprimir en una lista los numeros desde el 1 al numero introducido
// en la variable numero
// echo "<ul>";
// echo "<li>1</li>";
// echo "<li>2</li>";
// echo "<li>3</li>";
// echo "<li>4</li>";
// echo "<li>5</li>";
// echo "</ul>";

// lo voy a realizar con un for
echo "<ul>";
for ($contador = 1; $contador <= $numero; $contador++) {
    echo "<li>{$contador}</li>";
}
echo "</ul>";

?>

<ul>
    <?php
    for ($contador = 1; $contador <= $numero; $contador++) {
    ?>
        <li><?= $contador ?></li>
    <?php
    }
    ?>
</ul>